package br.com.apicartoes.controllers;

import br.com.apicartoes.models.Usuario;
import br.com.apicartoes.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario cadastrarUsuario(@RequestBody @Valid Usuario usuario){
        Usuario usuarioObjeto = usuarioService.salvarUsuario(usuario);

        return  usuarioObjeto;
    }

    @GetMapping("/{id}")
    public Usuario consultarPorId (@PathVariable(name = "id") Long id){
        try{
            Usuario usuario = usuarioService.buscarUsuarioPorId(id);
            return usuario;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }




}

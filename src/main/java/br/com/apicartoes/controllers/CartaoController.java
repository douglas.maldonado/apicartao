package br.com.apicartoes.controllers;

import br.com.apicartoes.models.Cartao;
import br.com.apicartoes.models.Usuario;
import br.com.apicartoes.models.dtos.EntradaDTOCartao;
import br.com.apicartoes.models.dtos.RespostaDTOCartao;
import br.com.apicartoes.services.CartaoService;
import br.com.apicartoes.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaDTOCartao cadastrarCartao(@RequestBody @Valid EntradaDTOCartao entradaDTOCartao){
        Cartao cartao = new Cartao();

        Usuario usuario = usuarioService.buscarUsuarioPorId(entradaDTOCartao.getClienteId());
        cartao.setUsuario(usuario);
        cartao.setNumero(entradaDTOCartao.getNumero());

        Cartao cartaoObjeto = cartaoService.salvarCartao(cartao);

        RespostaDTOCartao respostaDTOCartao = new RespostaDTOCartao();
        respostaDTOCartao.setId(cartaoObjeto.getId());
        respostaDTOCartao.setNumero(cartaoObjeto.getNumero());
        respostaDTOCartao.setClienteId(cartaoObjeto.getUsuario().getId());
        respostaDTOCartao.setAtivo(cartaoObjeto.isAtivo());

        return respostaDTOCartao;
    }

    @PatchMapping("/{numero}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public RespostaDTOCartao ativarCartao(@PathVariable(name = "numero") Long numero, @RequestBody Cartao cartao){
        Cartao cartaoObjeto = cartaoService.ativarCartao(numero, cartao.isAtivo());

        RespostaDTOCartao respostaDTOCartao = new RespostaDTOCartao();
        respostaDTOCartao.setId(cartaoObjeto.getId());
        respostaDTOCartao.setNumero(cartaoObjeto.getNumero());
        respostaDTOCartao.setClienteId(cartaoObjeto.getUsuario().getId());
        respostaDTOCartao.setAtivo(cartaoObjeto.isAtivo());

        return respostaDTOCartao;
    }

    @GetMapping("/{numero}")
    public RespostaDTOCartao consultarCartao(@PathVariable(name = "numero") Long numero){
        Cartao cartaoObjeto = cartaoService.buscarCartaoPorNumero(numero);

        RespostaDTOCartao respostaDTOCartao = new RespostaDTOCartao();
        respostaDTOCartao.setId(cartaoObjeto.getId());
        respostaDTOCartao.setNumero(cartaoObjeto.getNumero());
        respostaDTOCartao.setClienteId(cartaoObjeto.getUsuario().getId());
        respostaDTOCartao.setAtivo(cartaoObjeto.isAtivo());

        return respostaDTOCartao;
    }
}

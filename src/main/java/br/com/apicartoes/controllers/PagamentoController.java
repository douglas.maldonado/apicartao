package br.com.apicartoes.controllers;

import br.com.apicartoes.models.Cartao;
import br.com.apicartoes.models.Pagamento;
import br.com.apicartoes.models.dtos.EntradaDTOPagamento;
import br.com.apicartoes.models.dtos.RespostaDTOPagamento;
import br.com.apicartoes.services.CartaoService;
import br.com.apicartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaDTOPagamento cadastrarPagamento (@RequestBody @Valid EntradaDTOPagamento entradaDTOPagamento){
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(entradaDTOPagamento.getDescricao());
        pagamento.setValor(entradaDTOPagamento.getValor());

        Cartao cartao = cartaoService.buscarCartaoPorId(entradaDTOPagamento.getCartao_id());
        pagamento.setCartao(cartao);

        Pagamento pagamentoObjeto = pagamentoService.salvarPagamento(pagamento);

        RespostaDTOPagamento respostaDTOPagamento = new RespostaDTOPagamento();
        respostaDTOPagamento.setId(pagamentoObjeto.getId());
        respostaDTOPagamento.setCartao_id(pagamentoObjeto.getCartao().getId());
        respostaDTOPagamento.setDescricao(pagamentoObjeto.getDescricao());
        respostaDTOPagamento.setValor(pagamentoObjeto.getValor());

        return respostaDTOPagamento;
    }

    @GetMapping("/pagamentos/{id_cartao}")
    public List<RespostaDTOPagamento> consultarPagamentos(@PathVariable(name = "id_cartao") Long id){
        List<Pagamento> pagamentos = new ArrayList<>();
        Iterable<Pagamento> pagamentoIterable = pagamentoService.consultarPagamentosPorCartao(id);

        pagamentoIterable.forEach(pagamentos::add);

        List<RespostaDTOPagamento> respostaDTOPagamentos = new ArrayList<>();

        for(Pagamento pagamento: pagamentos) {
            RespostaDTOPagamento respostaDTOPagamento = new RespostaDTOPagamento();
            respostaDTOPagamento.setId(pagamento.getId());
            respostaDTOPagamento.setCartao_id(pagamento.getCartao().getId());
            respostaDTOPagamento.setDescricao(pagamento.getDescricao());
            respostaDTOPagamento.setValor(pagamento.getValor());

            respostaDTOPagamentos.add(respostaDTOPagamento);
        }

        return respostaDTOPagamentos;
    }

}

package br.com.apicartoes.services;

import br.com.apicartoes.models.Usuario;
import br.com.apicartoes.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario salvarUsuario(Usuario usuario){

        Usuario usuarioObjeto = usuarioRepository.save(usuario);

        return usuarioObjeto;
    }

    public Usuario buscarUsuarioPorId(Long id){

        Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);

        if(optionalUsuario.isPresent()){
            return  optionalUsuario.get();
        }

        throw new RuntimeException("Usuario não cadastrado");

    }

}

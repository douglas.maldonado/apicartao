package br.com.apicartoes.services;

import br.com.apicartoes.models.Cartao;
import br.com.apicartoes.models.Usuario;
import br.com.apicartoes.models.dtos.EntradaDTOCartao;
import br.com.apicartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private UsuarioService usuarioService;

    public Cartao salvarCartao(Cartao cartao){

        Usuario usuario = usuarioService.buscarUsuarioPorId(cartao.getUsuario().getId());
        cartao.setUsuario(usuario);
        cartao.setAtivo(false);

        Cartao cartaoObjeto = cartaoRepository.save(cartao);
        return cartaoObjeto;
    }

    public Cartao ativarCartao(Long numero, boolean ativo){
        Cartao cartaoObjeto = buscarCartaoPorNumero(numero);
        cartaoObjeto.setAtivo(ativo);

        Cartao cartaoRetorno = cartaoRepository.save(cartaoObjeto);

        return cartaoRetorno;
    }

    public Cartao buscarCartaoPorNumero(Long numero){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);

        if(cartaoOptional.isPresent()){
            return cartaoOptional.get();
        }

        throw new RuntimeException("Cartão não cadastrado");
    }

    public Cartao buscarCartaoPorId(Long id) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);

        if(cartaoOptional.isPresent()){
            return cartaoOptional.get();
        }

        throw new RuntimeException("Cartão não cadastrado");
    }
}

package br.com.apicartoes.services;

import br.com.apicartoes.models.Cartao;
import br.com.apicartoes.models.Pagamento;
import br.com.apicartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoService cartaoService;

    public Pagamento salvarPagamento(Pagamento pagamento){
        Cartao cartao = cartaoService.buscarCartaoPorId(pagamento.getCartao().getId());
        pagamento.setCartao(cartao);

        Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);

        return pagamentoObjeto;
    }

    public Iterable<Pagamento> consultarPagamentosPorCartao(Long id){
        Cartao cartao = cartaoService.buscarCartaoPorId(id);

        Iterable<Pagamento> pagamentoIterable = pagamentoRepository.findAllByCartao(cartao);

        return pagamentoIterable;
    }
}

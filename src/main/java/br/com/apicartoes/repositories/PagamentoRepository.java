package br.com.apicartoes.repositories;

import br.com.apicartoes.models.Cartao;
import br.com.apicartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {
    Iterable<Pagamento> findAllByCartao(Cartao cartao);
}

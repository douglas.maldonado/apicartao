package br.com.apicartoes.repositories;

import br.com.apicartoes.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {
}
